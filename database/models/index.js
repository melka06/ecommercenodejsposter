'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(__filename);
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/database.json')[env];
var db        = {};


if (config.use_env_variable) {
  var sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  var sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

// Ajout des association entre les models.
db.Produit.belongsTo(db.User); // un produit appartient à un utilisateur
db.User.hasMany(db.Produit); // un utilisateur à plusieurs produits

db.Produit.belongsTo(db.Categorie); // un produit appartient à une categorie
db.Categorie.hasMany(db.Produit); // une categorie à plusieurs produit

db.Role.hasMany(db.User); // un role est associé à plusieurs user
db.User.belongsTo(db.Role); // un utilisateur appartient à un role

module.exports = db;
