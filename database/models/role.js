'use strict';
module.exports = (sequelize, DataTypes) => {
  var Role = sequelize.define('Role', {
    nom: DataTypes.STRING,
    valeur: DataTypes.STRING
  }, {underscored: true});
  return Role;
};