'use strict';
module.exports = (sequelize, DataTypes) => {
  var Produit = sequelize.define('Produit', {
    nom: DataTypes.STRING,
    description: DataTypes.TEXT,
    image: DataTypes.STRING,
    prix: DataTypes.INTEGER
  }, {underscored: true});
  
  return Produit;
};