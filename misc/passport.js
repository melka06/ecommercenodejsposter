var LocalStrategy   = require('passport-local').Strategy;
var models          = require('../database/models');

module.exports = function(passport) {

    // Utilisé pour serialiser l'user dans la session.
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // Utilisé pour deserialiser l'user dans la session.
    passport.deserializeUser(function(id, done) {
        // On recup l'user depuis la db
        models.User.findById(id).then(user => {
            // l'user existe
            if(user){
                done(null, user);
            }else{
                done("Erreur utilisateur inexistant (deserializeUser)", user);
            }
        });
    });

    // "Strategie" pour se connecter.
    passport.use('local-login', new LocalStrategy({
        usernameField: 'email', // changement du nom du champs username en email
        passwordField: 'password',
        passReqToCallback: true
    },
    function(req, email, password, done) { 
        // on execute la fonction apres le prochaine tick de la boucle evenementielle.
        process.nextTick(function() {
            console.log(req.body)
            // On vérifie qu'un utilisateur non revoquer existe.
            models.User.findOne({where:{ email :  email, password:password }}).then(user=>{
                if(user){ // existe
                    if(user.actif == 0)
                        done(null, false, req.flash('loginMessage', 'Compte désactivé.'));

                    done(null, user);
                }else{  // !existe
                    done(null, false, req.flash('loginMessage', 'Identifiant incorrect.'));
                }
            });
        });
    }));


    // "Strategie" pour se connecter.
    passport.use('local-signup', new LocalStrategy({
        usernameField: 'email', // changement du nom du champs username en email
        passwordField: 'password',
        passReqToCallback: true
    },
    function(req, email, password, done) {
        // on execute la fonction apres le prochaine tick de la boucle evenementielle.
        process.nextTick(function() {
            // On vérifie l'existance d'un user @ la meme adresse
            models.User.findOne({ where: { email :  email }}).then(user => {
                if(user){
                    return done(null, false, req.flash('signupMessage', 'Un utilisateur existe deja.'));
                }else{
                    console.log(email);
                    console.log(password);

                    // on crée l'utilisateur
                    models.User.create({
                        nom:req.body.nom,
                        prenom:req.body.prenom,
                        email: email,
                        password: password,
                        role_id: 2 //utilisateur
                    }).then(user =>{
                        if(user)
                            done(null, user);
                    });
                }
            });
        });

    }));
};
