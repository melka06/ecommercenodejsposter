var config = require("./config"); // config
var jsonWebToken = require('jsonwebtoken'); // jwt

module.exports = {
	/**
	* On recupere le payload du token.
	*/
	getDataFromToken: function(token){
		return jsonWebToken.verify(token, config.secretKeyToken);
	},

	/**
	* Renvoi un token
	* param : datas array de donnés
	*/
	createToken: function(idUser, admin){
		var datas = {
			id: idUser,
			admin: admin
		};
		return jsonWebToken.sign(datas, config.secretKeyToken);         
	},

	/**
	* Renvoi un boolean qui indique si le payload du token contient l'élément "admin" à 1.
	*/
	isAdmin: function(token){ // A revoir
		var decoded = jsonWebToken.verify(token, config.secretKeyToken);

		if (!decoded) {
			throw("Impossible de decodé le token"); 
		}

		// console.log(decoded);

		return (decoded.admin === true) ? true : false;
	}
}
