var express 			= require('express');
var application 		= express();
var bodyParser 			= require('body-parser');
var config 				= require("./misc/config"); // fichier de configuration de l'app
var enRouten 			= require('express-enrouten'); 
var models 				= require('./database/models');

var flash    = require('connect-flash');
var passport = require('passport');
var cookieParser = require('cookie-parser');
var session      = require('express-session');

// config
application.set('view engine', 'ejs');
application.set('views', __dirname + '/views');

// required for passport
application.use(flash()); 
application.use(session({ secret: 'mohameddemahom' }));

application.use(passport.initialize());
application.use(passport.session());

require('./misc/passport')(passport);

// Middleware qui nous permet de récupérer des infos qui seront dans le header
application.use(function (req, res, next) {
	// On détermine si une session existe.
    res.locals.login = req.isAuthenticated();
    //res.locals.admin = req.user.role_id;

    // si l'utilisateur est loggé.
    if(req.isAuthenticated()){
    	// on recupere des valeurs pour le header.
    	res.locals.email = req.user.email;
    	res.locals.admin = (req.user.role_id == 1) ? true : false;
    }
    
    // On récupère les categories pour les dropdown dans le head.
    models.Categorie.findAll().then(categories=>{
		res.locals.categories =categories;
		next();
    });    
});

application.use(cookieParser()); // read cookies (needed for auth)
application.use(bodyParser.urlencoded({ extended: false }));
application.use(enRouten({ directory: 'routes' }));


// création de la structure de la base de données
models.Role.sync({force:true})
	.then(function(){return models.User.sync({force:true});})
	.then(function(){return models.Categorie.sync({force:true});})
	.then(function(){return models.Produit.sync({force:true});})
	.then(function(){
		models.Role.create({
			nom: "Administrateur",
			valeur: "ADMIN"
		});
	})
	.then(function(){
		models.Role.create({
			nom: "Utilisateur",
			valeur: "USER"
		});
	})
	.then(function(){
		models.User.create({
			nom: "admin",
		    prenom: "admin",
		    email: "admin@admin.com",
		    password: "admin",
		    actif: 1,
		    role_id: 1
		});
	})
	.then(function(){
		models.User.create({
			nom: "test",
		    prenom: "test",
		    email: "test@test.com",
		    password: "test",
		    actif: 1,
		    role_id: 2
		});
	})
	.then(function(){
		models.Categorie.create({
			nom: "Simpson",
		    description: "Produit Simpson"
		});
	})
	.then(function(){
		models.Categorie.create({
			nom: "Nemo",
		    description: "Produit Nemo"
		});
	})
	.then(function(){
		models.Categorie.create({
			nom: "RIO",
		    description: "Produit RIO"
		});
	})
	.then(function(){
		models.Produit.create({
			nom: "Simpson poster 1",
		    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et lacinia enim. Etiam ante elit, tempus nec sapien cursus, ornare consequat dolor. In hac habitasse platea dictumst. Nunc eu mi sollicitudin, sagittis dui eu, tincidunt quam. Sed varius enim lorem.",
		    image: "http://lorempicsum.com/simpsons/627/500/3",
		    prix: 100,
		    user_id: 1,
		    categorie_id: 1
		});
	})
	.then(function(){
		models.Produit.create({
			nom: "Simpson poster 2",
		    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et lacinia enim. Etiam ante elit, tempus nec sapien cursus, ornare consequat dolor. In hac habitasse platea dictumst. Nunc eu mi sollicitudin, sagittis dui eu, tincidunt quam. Sed varius enim lorem.",
		    image: "http://lorempicsum.com/simpsons/627/500/1",
		    prix: 180,
		    user_id: 1,
		    categorie_id: 1
		});
	})
	.then(function(){
		models.Produit.create({
			nom: "Simpson poster 6",
		    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et lacinia enim. Etiam ante elit, tempus nec sapien cursus, ornare consequat dolor. In hac habitasse platea dictumst. Nunc eu mi sollicitudin, sagittis dui eu, tincidunt quam. Sed varius enim lorem.",
		    image: "http://lorempicsum.com/simpsons/627/500/5",
		    prix: 18,
		    user_id: 1,
		    categorie_id: 1
		});
	})
	.then(function(){
		models.Produit.create({
			nom: "Nemo poster 1",
		    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et lacinia enim. Etiam ante elit, tempus nec sapien cursus, ornare consequat dolor. In hac habitasse platea dictumst. Nunc eu mi sollicitudin, sagittis dui eu, tincidunt quam. Sed varius enim lorem.",
		    image: "http://lorempicsum.com/nemo/627/500/3",
		    prix: 100,
		    user_id: 1,
		    categorie_id: 2
		});
	})
	.then(function(){
		models.Produit.create({
			nom: "Nemo poster 2",
		    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et lacinia enim. Etiam ante elit, tempus nec sapien cursus, ornare consequat dolor. In hac habitasse platea dictumst. Nunc eu mi sollicitudin, sagittis dui eu, tincidunt quam. Sed varius enim lorem.",
		    image: "http://lorempicsum.com/nemo/627/500/1",
		    prix: 180,
		    user_id: 1,
		    categorie_id: 2
		});
	})
	.then(function(){
		models.Produit.create({
			nom: "Nemo poster 6",
		    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et lacinia enim. Etiam ante elit, tempus nec sapien cursus, ornare consequat dolor. In hac habitasse platea dictumst. Nunc eu mi sollicitudin, sagittis dui eu, tincidunt quam. Sed varius enim lorem.",
		    image: "http://lorempicsum.com/nemo/627/500/5",
		    prix: 18,
		    user_id: 1,
		    categorie_id: 2
		});
	})
	.then(function(){
		models.Produit.create({
			nom: "RIO poster 1",
		    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et lacinia enim. Etiam ante elit, tempus nec sapien cursus, ornare consequat dolor. In hac habitasse platea dictumst. Nunc eu mi sollicitudin, sagittis dui eu, tincidunt quam. Sed varius enim lorem.",
		    image: "http://lorempicsum.com/rio/627/500/3",
		    prix: 100,
		    user_id: 1,
		    categorie_id: 3
		});
	})
	.then(function(){
		models.Produit.create({
			nom: "RIO poster 2",
		    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et lacinia enim. Etiam ante elit, tempus nec sapien cursus, ornare consequat dolor. In hac habitasse platea dictumst. Nunc eu mi sollicitudin, sagittis dui eu, tincidunt quam. Sed varius enim lorem.",
		    image: "http://lorempicsum.com/rio/627/500/1",
		    prix: 180,
		    user_id: 1,
		    categorie_id: 3
		});
	})
	.then(function(){
		models.Produit.create({
			nom: "RIO poster 6",
		    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et lacinia enim. Etiam ante elit, tempus nec sapien cursus, ornare consequat dolor. In hac habitasse platea dictumst. Nunc eu mi sollicitudin, sagittis dui eu, tincidunt quam. Sed varius enim lorem.",
		    image: "http://lorempicsum.com/rio/627/500/5",
		    prix: 18,
		    user_id: 1,
		    categorie_id: 3
		});
	});

application.listen(config.port);