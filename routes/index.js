var models = require("../database/models");
var passport = require("passport");

module.exports = function (router) {
    
    /**
	* Affiche la page d'accueil
    */ 
    router.get('/', function( req, res ){
    	// on récupère les info du produit
    	models.Produit.findAll({limit:10}).then(function (produits) {
 			if( produits != null){			
				res.render("index", {error : false, produits : produits });
		    }else{
		        res.render("index", {error : true, message : "Le produit n'existe pas"});
		    }
		});   
    }); 

    /**
    * Page login
    */
    router.get('/login', function( req, res ){
        res.render("login", { message: req.flash('loginMessage') });          
    });

    router.post('/login', passport.authenticate('local-login', {
        successRedirect : '/dashuser', 
        failureRedirect : '/login',
        failureFlash : true 
    }));

    /**
    * Page inscription
    */
    router.get('/inscription', function( req, res ){
        res.render("inscription", { message: req.flash('signupMessage') });          
    });

    router.post('/inscription', passport.authenticate('local-signup', {
        successRedirect : '/dashuser',
        failureRedirect : '/inscription', 
        failureFlash : true
    }));

    /**
    * Page deconnexion
    */
    router.get('/logout', function( req, res ){
        req.logout();
        res.redirect('/');
    });
};