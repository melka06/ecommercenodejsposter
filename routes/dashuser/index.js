var models = require("../../database/models");

module.exports = function (router) { 
 
    function isUser(req, res, next) {

        // Si l'utilisateur est connecté.
        if (req.isAuthenticated())
            return next();

        // redirection vers login.
        res.redirect('/login');
    }

    // On utilise le middleware isUser pour proteger toute les pages dashUser
    router.use(isUser);


    /**
    * Voir le profil de l'utlisateur
    */ 
    router.get('/', function( req, res ){
        var id = req.user.id;

        models.User.findById(id, {raw: true}).then(function(utilisateur){
            if(utilisateur != null){
                res.render("dashboard/profil", {utilisateur: utilisateur});
            }else{
                res.redirect("/");
            }
        });        
    });

    /**
    * Formulaire pour éditer le profil de l'utlisateur
    */ 
    router.get('/edit',  function( req, res ){
        var id = req.user.id;

        models.User.findById(id, {raw: true}).then(function(utilisateur){
            if(utilisateur != null){
                console.log(utilisateur);
                res.render("dashboard/editprofil", {utilisateur: utilisateur});
            }else{
                res.redirect("/",);
            }
        });

    }); 

    /**
    * Editer le profil de l'utlisateur
    */ 
    router.post('/edit',  function( req, res ){
        var id = req.user.id;

        var newValue = {
            nom: req.body.nom,
            prenom: req.body.prenom,
        }

        // si le password a été modifié dans les champs
        if(req.body.password != "")
            newValue["password"] = req.body.password;

        // on vérifie si l'user à modifier existe 
        models.User.findById(id).then(user => {
            if (user == null){
                // L'user n'existe pas.
                res.redirect("/");
            }else{
                // on update l'entité
                models.User.update(newValue, {
                    where: {
                        id: id
                    }
                });

                // on récupere les information ajouté
                models.User.findById(id).then(user => {
                    // on affiche la page profil.
                    res.render("dashboard/profil", {utilisateur: user});
                });                
            }
        });        
    }); 

    /**
    * Suppresion du profil de l'utilisateur
    */ 
    router.post('/delete',  function( req, res ){
        var id = req.user.id;

        models.User.findById (id).then(utilisateur => {
            if(utilisateur != null){
                utilisateur.destroy();
                req.logout();
                res.redirect("/");
            }
        });
    }); 
}