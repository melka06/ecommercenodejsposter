var models = require("../../database/models");

module.exports = function (router) {
    
    /**
	* Voir une categorie grace à son identifiant.
    */ 
    router.get('/:id', function( req, res ){
    	var id = parseInt(req.params.id);

    	// on récupère les info de la catégorie
    	models.Categorie.findById(id).then(categorie => {
 			if( categorie != null){
 				// on recupere les produits de la categorie
				models.Produit.findAll({where : {categorie_id : id }}).then(produits => {
		            if( produits.length != 0){
						res.render("categorie", {error : false, categorie : categorie, produits : produits});
		            }else{
		            	res.render("categorie", {error : true, message : "Aucun produit dans cette categorie"});
		            }
		        });  
            }else{
            	res.render("categorie", {error : true, message : "La categorie n'existe pas."});
            }
 		});         	
    }); 

}