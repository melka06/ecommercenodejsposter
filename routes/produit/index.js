var models = require("../../database/models");

module.exports = function (router) {
    
    /**
	* Voir un produit grace à son identifiant.
    */ 
    router.get('/:id', function( req, res ){
    	var id = parseInt(req.params.id);

    	// on récupère les info du produit
    	models.Produit.findById(id).then(function (produit) {
 			if( produit != null){			
				res.render("produit", {error : false, produit : produit });
		    }else{
		        res.render("produit", {error : true, message : "Le produit n'existe pas"});
		    }
		});   
    }); 

}