var models = require("../../../database/models");

module.exports = function (router) {
    
    /**
    * Afficher tous les categories
    */ 
    router.get('/', function( req, res ){
    	models.Categorie.findAll().then(function(categories){
            if(categories != null){
                res.render("dashboard/categories", { categories: categories });
            }else{
                res.redirect("/");
            }
        });        
    });

    /**
    * Supprimer une catégorie
    */ 
    router.get('/delete/:id', function( req, res ){
    	var id = parseInt(req.params.id);

        models.Categorie.findById(id).then(function(categorie){
            if(categorie != null){
            	categorie.destroy();

				res.redirect("/dashadmin/categorie");
            }else{
				res.redirect("/dashboard/categorie");
            }
        });        
    });

    /**
    * Editer une catégorie
    */ 
    router.get('/edit/:id', function( req, res ){
    	var id = parseInt(req.params.id);

        models.Categorie.findById(id).then(function(categorie){
            if(categorie != null){
            	// on recupere les categories
	            res.render("dashboard/editcategorie", {categorie: categorie});
            }else{
				res.redirect("/dashadmin/categorie");
            }
        });        
    });

    /*
    * met à jour de la categorie
    */
    router.post('/edit/:id', function( req, res ){
    	var id = parseInt(req.params.id);
    	var newValue = req.body;

    	// on recupere le produit
        models.Categorie.findById(id).then(categorie => {
            if(categorie != null){
            	models.Categorie.update(newValue, {
                    where: {
                        id: id
                    }
                });

            	// la categorie existe, on la met à jour
				res.redirect("/dashadmin/categorie");
            }else{
				res.redirect("/dashadmin/categorie");
            }
        }); 
    });

    /**
	* Afficher formulaire ajout d'une categorie
    */
	router.get('/add', function( req, res ){
		models.Categorie.findAll().then(categorie => {
            // on affiche le formulaire pour ajouter une categorie
 			res.render("dashboard/addcategorie", {categorie : categorie});
        });
    });

    /**
	* Ajout d'une categorie
    */
	router.post('/add', function( req, res ){
		var data = req.body;
		// on verifie qu'un produit n'existe pas avec le meme nom
        models.Categorie.findOrCreate(
        	{where: data}
        ).spread((categorie, created) => {
		    if(created != false){
		    	// redirection vers la page du produit
				res.redirect("/dashadmin/categorie");
		    }else{
		    	// le produit existe deja
		    	res.redirect("/dashadmin/categorie");
		    }
 		});
    });
}