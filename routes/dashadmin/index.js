var models = require("../../database/models");

module.exports = function (router) {   

    function isAdmin(req, res, next) {

        // Si l'utilisateur est loggé est qu'il possede le role 1.
        if (req.isAuthenticated() && req.user.role_id == 1)
            return next();

        // Redirection dans le cas contraire.
        res.redirect('/login');
    }

    // On utilise le middleware isUser pour proteger toute les pages dashAdmin.
    router.use(isAdmin);

    /**
    * Afficher tous les utilisateurs
    */ 
    router.get('/', function( req, res ){
        models.User.findAll().then(function(utilisateurs){
            if(utilisateurs != null){
                res.render("dashboard/index", {error:false, success:false, utilisateurs: utilisateurs});
            }else{
                res.redirect("/",);
            }
        });        
    });
}