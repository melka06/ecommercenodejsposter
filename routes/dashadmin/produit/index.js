/*voir les produits -> get 
supprimer un produit -> post delete
ajouter un produit -> get formulaire
ajouter un produit -> post ajout*/

var models = require("../../../database/models");

module.exports = function (router) {
    
    /**
    * Afficher tous les produits
    */ 
    router.get('/', function( req, res ){
    	 var error = (req.query.error!=null) ? true:false;
    	 var success = (req.query.success!=null) ? true:false;
    	 var message = req.query.message;

        models.Produit.findAll().then(function(produits){
            if(produits != null){
                res.render("dashboard/produits", {error:error, success:success, message:message, produits: produits});
            }else{
                res.redirect("/");
            }
        });        
    });

    /**
    * Supprimer un produit
    */ 
    router.get('/delete/:id', function( req, res ){
    	var id = parseInt(req.params.id);

        models.Produit.findById(id).then(function(produit){
            if(produit != null){
            	produit.destroy();

            	// on recupere les produits pour la page des produits
				models.Produit.findAll().then(produits =>{
					res.redirect("/dashadmin/produit?success=" + "Produit supprimé");
				});

            }else{
				res.redirect("/dashadmin/produit?error=" + "Le produit n'existe pas");
            }
        });        
    });

      /**
    * Editer un produit
    */ 
    router.get('/edit/:id', function( req, res ){
    	var id = parseInt(req.params.id);

        models.Produit.findById(id).then(function(produit){
            if(produit != null){
            	// on recupere les categories
            	models.Categorie.findAll().then(categories => {
            		// le produit existe, on affiche le formulaire pour modifier le produit
            		console.log(categories);
	            	res.render("dashboard/editproduit", {produit: produit, categories: categories});
            	});
            }else{
				res.redirect("/dashadmin/produit");
            }
        });        
    });

    /*
    * met à jour le produit
    */
    router.post('/edit/:id', function( req, res ){
    	var id = parseInt(req.params.id);
    	var newValue = req.body;

    	// on recupere le produit
        models.Produit.findById(id).then(produit => {
            if(produit != null){
            	models.Produit.update(newValue, {
                    where: {
                        id: id
                    }
                });

            	// le produit existe, on le met à jour
				res.redirect("/dashadmin/produit");
            }else{
				res.redirect("/dashadmin/produit");
            }
        }); 
    });

    /**
	* Afficher formulaire ajout d'un produit
    */
	router.get('/add', function( req, res ){
		models.Categorie.findAll().then(categories => {
            // le produit existe, on affiche le formulaire pour modifier le produit
 			res.render("dashboard/addproduit", {categories : categories});
        });
    });

    /**
	* Ajout d'un produit
    */
	router.post('/add', function( req, res ){
		var data = req.body;
		// on verifie qu'un produit n'existe pas avec le meme nom
        models.Produit.findOrCreate(
        	{where: data}
        ).spread((produit, created) => {
		    if(created != false){
		    	// redirection vers la page du produit
				res.redirect("/produit/"+produit.id);
		    }else{
		    	// le produit existe deja
		    	res.redirect("/dashadmin/produit");
		    }
 		});
    });
}