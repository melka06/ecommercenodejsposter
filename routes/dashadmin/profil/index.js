/*voir les utilisateurs -> get 

supprimer un utilisateurs -> post delete

editer un utilisateurs -> get formulaire
editer un utilisateurs -> post changement


révoquer un utilisateurs -> post edit*/

var models = require("../../../database/models");

module.exports = function (router) {
    

    /**
    * Supprimer un utilisateur
    */ 
    router.get('/delete/:id', function( req, res ){
    	var id = parseInt(req.params.id);

        models.User.findById(id).then(function(user){
            if(user != null){
            	user.destroy();

            	// on recupere les produits pour la page des produits
				models.Produit.findAll().then(produits =>{
					res.redirect("/dashadmin/?success=" + "Produit supprimé");
				});

            }else{
				res.redirect("/dashadmin/?error=" + "Le produit n'existe pas");
            }
        });        
    });

    /**
    * Editer un utilisateur
    */ 
    router.get('/edit/:id', function( req, res ){
    	var id = parseInt(req.params.id);

        models.User.findById(id).then(function(user){
            if(user != null){
            	// on recupere les roles
            	models.Role.findAll().then(roles => {
            		// l'user existe, on affiche le formulaire pour modifier le produit
	            	res.render("dashboard/editprofiladmin", {utilisateur: user, roles: roles});
            	});
            }else{
				res.redirect("/dashadmin");
            }
        });        
    });

    /*
    * met à jour le produit
    */
    router.post('/edit/:id', function( req, res ){
    	var id = parseInt(req.params.id);
    	var newValue = req.body;

    	// on recupere l'utilisateur
        models.User.findById(id).then(function(user){
            if(user != null){
                // le profil existe, on le met à jour
            	models.User.update(newValue, {
                    where: {
                        id: id
                    }
                });

				res.redirect("/dashadmin");
            }else{
				res.redirect("/dashadmin");
            }
        }); 
    });


    /**
    * révoquer un utilisateur
    */
    router.get('/revoquer/:id', function( req, res ){
        var id = parseInt(req.params.id);

        // on verifie que l'utilisateur existe
       models.User.findById(id).then(function(user){
            if(user != null){
                var newValue = {actif:0};

                models.User.update(newValue, {
                    where: {
                        id: id
                    }
                });
               
                res.redirect("/dashadmin");
            }else{
                res.redirect("/dashadmin");
            }
        }); 
    });

    /**
    * rétablir un utilisateur un utilisateur
    */
    router.get('/retablir/:id', function( req, res ){
        var id = parseInt(req.params.id);

        // on verifie que l'utilisateur existe
       models.User.findById(id).then(function(user){
            if(user != null){
                var newValue = {actif:1};

                models.User.update(newValue, {
                    where: {
                        id: id
                    }
                });
               
                res.redirect("/dashadmin");
            }else{
                res.redirect("/dashadmin");
            }
        }); 
    });
}